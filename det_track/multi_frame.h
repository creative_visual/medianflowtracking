
#ifndef _MULTI_FRAME_
#define _MULTI_FRAME_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>

#include <vector>
#include <queue>
#include <unordered_map>
#include <set>
#include <list>

#include <iomanip>



class Loc{
public:
  int x;
  int y;
  int w;
  int h;
  int unique_key;

  Loc()
  {
    x=y=w=h=0;
  }

  Loc(int _x, int _y, int _w, int _h)
    : x(_x)
    , y(_y)
    , w(_w)
    , h(_h)
  { 
    unique_key = y*w + x;
  }

  int key()
  {
    return (y*w + x);
  }

  void output()
  {
    std::cout << "x=" << x << ", y=" << y
      //<< ", w=" << w << ", h=" << h 
      << std::endl;
  }

};

class Object{
public:
  Loc loc;
  float scale;
  int frame_index;

  Object()
  {
    loc = Loc();
    scale=0;
    frame_index=0;
  }

  void output()
  {
    std::cout << "frame_index=" << frame_index << ", "
      << "scale=" << scale << std::endl << "    " ;
    loc.output();
  }
};

typedef std::unordered_map<int, std::vector<Object> > Loc_Obj_map;
typedef std::queue<std::vector<Object> > Obj_vec_que;

Loc get_Loc(int unique_key, int w, int h);
bool close_by(Loc self, int other_key, int r_thresh);
bool close_by(Loc self, Loc other, int r_thresh);
bool close_by(Object self, Object other, int r_thresh);

int produce_confirmed_object(std::vector<Object> new_detected, std::vector<Object>& confirmed, Obj_vec_que &queue, Loc_Obj_map &trace);
int produce_confirmed_object(std::vector<Object> new_detected, std::vector<Object>& confirmed);
void output_objects(std::vector<Object> detected, int i);
#endif