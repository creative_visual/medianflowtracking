#include "StdAfx.h"
#include "multi_frame.h"

/*
  This library takes detected object from multiple frames, which might contain 
  sudden changes due to detection stability, and produces smooth detection
  results
*/

static const int QUEUE_LENGTH = 5;
static const int merge_r_thresh = 20;
static const float multi_thresh = 0.5;  // threshold to decide multi-frame consistency
static const float ratio_thresh = 0.7;

Obj_vec_que temporal_queue, car_queue, ped_queue;
Loc_Obj_map object_trace, car_trace, ped_trace;

int image_width;
int image_height;

Loc get_Loc(int unique_key, int w, int h)
{
  int x = unique_key % w;
  int y = unique_key / w;
  Loc loc(x, y, w, h);
  return loc;
}

bool close_by(Loc self, int other_key, int r_thresh)
{
  int x = self.x;
  int y = self.y;
  int w = self.w;
  int tmp_x = other_key % w;
  int tmp_y = other_key / w;

  bool far_away = (abs(x-tmp_x)>r_thresh) || (abs(y-tmp_y)>r_thresh);
  return !far_away;
}

bool close_by(Loc self, Loc other, int r_thresh)
{
  int x = self.x;
  int y = self.y;
  int w = self.w;
  int d = self.h;
  int tmp_x = other.x;
  int tmp_y = other.y;

  bool far_away = (abs(x-tmp_x)>r_thresh) || (abs(y-tmp_y)>r_thresh);
  return !far_away;
}

bool close_by(Object self, Object other, float r_thresh)
{
  int x = self.loc.x;
  int y = self.loc.y;
  int d = self.scale;
  int tmp_x = other.loc.x;
  int tmp_y = other.loc.y;
  int tmp_d = other.scale;
  float xr = abs(((float)(x-tmp_x)) / ((float)std::min(d, tmp_d)));
  float yr = abs(((float)(y-tmp_y)) / ((float)std::min(d, tmp_d)));

  bool far_away = (xr>r_thresh) || (yr>r_thresh);
  return !far_away;
}

int merge_two_loc(Object& self, Object& other)
{
  // update 'self'
  self.scale = (self.scale + other.scale)/2;
  self.loc.x = (self.loc.x + other.loc.x)/2;
  self.loc.y = (self.loc.y + other.loc.y)/2;
  self.loc.unique_key = self.loc.y*self.loc.w + self.loc.x;
  // invalidate 'other'
  other.scale = 0;

  return 0;
}

void merge_detected(std::vector<Object>& new_detected)
{
  std::vector<Object>::iterator it_obj=new_detected.begin();
  for(;it_obj!=new_detected.end();)
  {
    if(it_obj->scale == 0)  // if previously-merged, ==0.
    {
      it_obj = new_detected.erase(it_obj);  // delete invalidate ones
      continue;
    }

    for(std::vector<Object>::iterator it_obj2 = it_obj+1; it_obj2!=new_detected.end();++it_obj2)
    {
      //if(close_by((it_obj->loc), (it_obj2->loc), merge_r_thresh*2))
      if(close_by((*it_obj), (*it_obj2), ratio_thresh))
      {
        merge_two_loc(*it_obj, *it_obj2);
        //it_obj2->scale = 0;
      }
    }
    ++it_obj;
  }
}

Loc_Obj_map re_cluster_trace(Loc_Obj_map object_trace)
{
  Loc_Obj_map new_trace;
  Loc_Obj_map::iterator it_map;

  for(it_map=object_trace.begin(); it_map!=object_trace.end(); it_map++)
  {
    // update location of this trace
    long long x_accu = 0;
    long long y_accu = 0;
    int n = 0;

    std::vector<Object> curr_trace = it_map->second;
#if 0
    std::vector<Object>::iterator it_obj = curr_trace.begin();
    for(; it_obj!=curr_trace.end(); it_obj++)
    {
      image_width = it_obj->loc.w;
      x_accu += it_obj->loc.x;
      y_accu += it_obj->loc.y;
      n++;
    }

    int new_key = (y_accu/n)*image_width + (x_accu/n);
#else
    int new_key = curr_trace.back().loc.y * curr_trace.back().loc.w + curr_trace.back().loc.x;  // update to closest frame
#endif
    new_trace[new_key] = curr_trace;
  }

  return new_trace;
}

int update_trace(Loc_Obj_map& object_trace, std::vector<Object> new_detected)
{
  Loc_Obj_map::iterator it_map;
  std::vector<Object>::iterator it_obj;

  merge_detected(new_detected);

  for(it_map=object_trace.begin(); it_map!=object_trace.end();)
  {
    // update old Objects in this trace (frame_index--, and remove that in the exiting frame)
    std::vector<Object>& curr_trace = it_map->second;
    it_obj = curr_trace.begin();
    for(; it_obj!=curr_trace.end();)
    {
      if(it_obj->frame_index==1)
        it_obj = curr_trace.erase(it_obj);
      else
      {
        (it_obj->frame_index)--;
        it_obj++;
      }
    }

    // if this trace is empty, remove it
    if(curr_trace.empty())
    {
      it_map = object_trace.erase(it_map);
      continue;
    }

    // deal with new incoming one
    bool containment_found = false;
    int curr_key = it_map->first;
    Object trace_object = it_map->second.back();
    it_obj=new_detected.begin();
    while(it_obj!=new_detected.end())
    {
      if(close_by((*it_obj), trace_object, ratio_thresh))
      {
        Object new_obj = *it_obj;
        new_obj.frame_index = QUEUE_LENGTH;  // index set as from the latest frame
        it_map->second.push_back(new_obj);

        containment_found = true;
        it_obj = new_detected.erase(it_obj);
        //break;
      }
      else
        ++it_obj;
    }

    it_map++;
  }

  it_obj=new_detected.begin();
  while(it_obj!=new_detected.end())
  {
    Object new_obj = *it_obj;
    new_obj.frame_index = QUEUE_LENGTH;  // index set as from the latest frame
    std::vector<Object> objects;
    objects.push_back(new_obj);

    int new_key = it_obj->loc.y*it_obj->loc.w + it_obj->loc.x;

    object_trace[new_key] = objects;
    it_obj++;
  }

  object_trace = re_cluster_trace(object_trace);

  return 0;
}

int trace_is_confirmed(std::vector<Object> curr)
{
  float N = (float)QUEUE_LENGTH;
  float M = (float)curr.size();
  return (M/N >= multi_thresh);
}

int produce_confirmed_object(std::vector<Object> new_detected, std::vector<Object>& confirmed)
{
  confirmed.clear();
  if(temporal_queue.size() < QUEUE_LENGTH)
  {
    temporal_queue.push(new_detected);
  }
  else
  {
    temporal_queue.pop();
    temporal_queue.push(new_detected);
  }

  Loc_Obj_map::iterator it_map;
  // update
  update_trace(object_trace, new_detected);

  if(temporal_queue.size() < QUEUE_LENGTH)
  {
    return 0;
  }

  // valid test
  for(it_map=object_trace.begin(); it_map!=object_trace.end(); it_map++)
  {
    if(trace_is_confirmed(it_map->second))
    {
      int w = it_map->second.front().loc.w;
      int h = it_map->second.front().loc.h;
      int x = it_map->first % w;
      int y = it_map->first / w;
      Loc new_loc(x, y, w, h);
      Object new_obj;
      new_obj.loc = new_loc;
      new_obj.frame_index = 1;
      new_obj.scale = it_map->second.front().scale;

      confirmed.push_back(new_obj);
    }
  }

  return 1;
}

int produce_confirmed_object(std::vector<Object> new_detected, std::vector<Object>& confirmed, Obj_vec_que &queue, Loc_Obj_map &trace)
{
	confirmed.clear();
	if(queue.size() < QUEUE_LENGTH)
	{
		queue.push(new_detected);
	}
	else
	{
		queue.pop();
		queue.push(new_detected);
	}

	Loc_Obj_map::iterator it_map;
	// update
	update_trace(trace, new_detected);

	if(queue.size() < QUEUE_LENGTH)
	{
		return 0;
	}

	// valid test
	for(it_map=trace.begin(); it_map!=trace.end(); it_map++)
	{
		if(trace_is_confirmed(it_map->second))
		{
			int w = it_map->second.front().loc.w;
			int h = it_map->second.front().loc.h;
			int x = it_map->first % w;
			int y = it_map->first / w;
			Loc new_loc(x, y, w, h);
			Object new_obj;
			new_obj.loc = new_loc;
			new_obj.frame_index = 1;
			new_obj.scale = it_map->second.front().scale;

			confirmed.push_back(new_obj);
		}
	}

	return 1;
}

void output_objects(std::vector<Object> detected, int i)
{
  std::cout<< "frame #" << i << std::endl;
  for(std::vector<Object>::iterator it = detected.begin();
    it!=detected.end();
    it++)
  {
    it->output();
  }
}