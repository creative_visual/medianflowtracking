// track2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <process.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <Windows.h>
#include <iostream>
#include <cstring>
#include <deque>
#include "multi_frame.h"

using namespace std;
using namespace cv;

#define  STARTTRACK -1
#define  DETECTED  0
#define  TRACKING  1
#define  TRACKED   2

struct rect
{
	int xmin;
	int ymin;
	int xmax;
	int ymax;
};

Mat g_cur_frame, g_image, g_track_img;
String g_tracker_algorithm = "MEDIANFLOW";//"MEDIANFLOW";
vector<Rect2d> g_boundingBox;
vector<int> g_tracked, g_thread_id;
int g_cur_obj_idx, g_cur_frame_idx, g_bworking;
int g_top, g_bottom, g_left, g_right;
int g_start_track_dist = 40;
int g_min_face = 18, g_max_face = 85;
rect g_det_roi, g_track_roi;
int g_ntracked = 5;

CRITICAL_SECTION cs,cs_hdl;

struct ObjectInfo
{
	int obj_id;  // 目标 id, 每个目标有唯一的 id 号
	Rect2d pos;  // 目标位置
};

struct FrameInfo 
{
	Mat frame;				//  图像数据
	int index;				//  帧索引
	vector<ObjectInfo> objs;  // 目标信息
};

struct ObjectTrack
{
	int obj_id;             // 物体 id
	int det_idx;            // 开始跟踪的帧索引号
	int tracked_idx;        // 表示该物体已经被跟踪到第 tracked_idx 帧
	rect pos;
	int flag;
};

vector<HANDLE> g_thread_handle;
deque<FrameInfo> g_frame_buf;
vector<ObjectTrack> g_obj_info;
int g_frame_buf_size = 10;
int g_ndelay = 3;
int g_min_tracked_idx = 1;
int g_maxsize = 100;
double g_move_rate = 1.7;

extern Obj_vec_que temporal_queue, car_queue, ped_queue;
extern Loc_Obj_map object_trace, car_trace, ped_trace;

int is_tracked();
Mat get_init_track_info(int &obj_id, rect &pos, int &init_frame_idx);
unsigned _stdcall track_single_obj(void *param);
void set_min_tracked_idx(int index);
int get_min_tracked_idx();
int get_next_frame(Mat &frame, int cur_idx);
void copy_roi_pxls(Mat &dst, Mat &src, int top, int bottom, int left, int right);
Mat get_init_local_img(Mat src, Rect2d &adjust_rect, Rect2d pos);
void get_next_local_pxls2(Mat& limg, Mat &src, Rect2d tar, Rect2d &adjust_rect, int maxsize);
void detect_face( Mat& img, CascadeClassifier& cascade, vector<rect> &faces );
int push_new_detected( vector<rect> &objs, int frame_idx, int &obj_cnt );
void set_tracked_info(int obj_id, Rect2d &pos, int tracked_idx);
double get_rect_overlap(rect* R1,rect* R2);
void smooth_detected( vector<rect> &objs, int width, int height, Obj_vec_que &queue, Loc_Obj_map &trace );
void erase_free_handle();

int _tmain(int argc, _TCHAR* argv[])
{
	String video_name = "D:\\video\\sz_ped5.mp4";//parser.get<String>( 1 );
	int start_frame = 509;//590;
	VideoCapture cap;
	cap.open( video_name );
	cap.set( CAP_PROP_POS_FRAMES, start_frame );

	int frameRate = cap.get(CV_CAP_PROP_FPS);
	int framenum = cap.get(CV_CAP_PROP_FRAME_COUNT);
	cv::Size frameSize = cv::Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),  
		(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));


	CascadeClassifier cascade("face_25.xml");

	g_det_roi.xmin = 0, g_det_roi.xmax = frameSize.width - 1, g_det_roi.ymin = 500, g_det_roi.ymax = 740;
	g_track_roi.xmin = 0, g_track_roi.xmax = frameSize.width - 1, g_track_roi.ymin = 740, g_track_roi.ymax = 1100;

	InitializeCriticalSection(&cs);
	InitializeCriticalSection(&cs_hdl);

	int width = frameSize.width, height = frameSize.height;
	g_top = 530, g_bottom = height - 1, g_left = 0, g_right = width - 1;
	int tmpw = g_right - g_left + 1, tmph = g_bottom - g_top + 1;

	Mat det_img(g_det_roi.ymax - g_det_roi.ymin+1, g_det_roi.xmax - g_det_roi.xmin+1, CV_8UC3);

	g_bworking = 1;
	cap >> g_cur_frame;
	g_cur_frame_idx = 0;

	VideoWriter writer;
	writer.open("tracked.avi", CV_FOURCC('D','I','V','X') , frameRate, frameSize, true);

	int obj_cnt = 0, thread_cnt = 0;
	DWORD st = GetTickCount();
	for ( ;; )
	{
		if( is_tracked() )
		{
			if( g_frame_buf.size() )
			{
				EnterCriticalSection(&cs);
				int min_track_idx = get_min_tracked_idx();  // 检查跟踪最慢的那个线程的进展情况，所有线程都已经跟踪完了第 min_track_idx 帧图像
				int ntracked = min_track_idx - g_frame_buf[0].index;
				if( ntracked >= g_ntracked || (ntracked < g_ntracked && g_frame_buf.size() == g_frame_buf_size) )
				{
					for(int i = 0; i < ntracked; i++)
					{
						for(int j = 0; j < g_frame_buf[0].objs.size(); j++)
						{
							rectangle( g_frame_buf[0].frame, g_frame_buf[0].objs[j].pos, Scalar( 0, 255, 255 ), 2); //  画目标所在的矩形框

							Point p1,p2;
							p1.x = 0, p1.y = g_track_roi.ymin, p2.x = width - 1, p2.y = g_track_roi.ymin;
							line(g_frame_buf[0].frame, p1, p2, Scalar( 0, 0, 255 ), 3);
						}

						//writer.write(g_frame_buf[0].frame);
						g_frame_buf.pop_front(); // 将已画完矩形框的队首图像从缓冲区删去
					}

					//imshow( "Tracking API", image );
				}
				LeaveCriticalSection(&cs);
			}

			if( g_frame_buf.size() < g_frame_buf_size  )
			{
				cap >> g_cur_frame;
				if(g_cur_frame.empty())break;

				vector<rect> faces;
				copy_roi_pxls(det_img, g_cur_frame, g_det_roi.ymin, g_det_roi.ymax, g_det_roi.xmin, g_det_roi.xmax); // 获取检测区域的像素
				detect_face(det_img, cascade, faces);  // 目标检测
				smooth_detected(faces, width, height, temporal_queue, object_trace);  // 多帧融合，平滑检测结果

				for(int i = 0; i < faces.size(); i++)
				{
					faces[i].xmin += g_det_roi.xmin, faces[i].xmax += g_det_roi.xmin;
					faces[i].ymin += g_det_roi.ymin, faces[i].ymax += g_det_roi.ymin;
				}

				EnterCriticalSection(&cs);
				g_cur_frame_idx++;
				//g_cur_frame.copyTo(g_image);

				FrameInfo frame_info;

				g_cur_frame.copyTo(frame_info.frame);
				frame_info.index = g_cur_frame_idx;
				g_frame_buf.push_back(frame_info);
				LeaveCriticalSection(&cs);

				int num = push_new_detected(faces, g_cur_frame_idx, obj_cnt);  // 是否有新的目标需要跟踪，返回要跟踪的目标数

				EnterCriticalSection(&cs_hdl);
				for(int i = 0; i < num; i++)
				{
					HANDLE handle = (HANDLE)_beginthreadex(NULL, 0, track_single_obj, NULL, NULL, NULL);  // 创建线程，跟踪目标
					g_thread_handle.push_back(handle);
				}
				LeaveCriticalSection(&cs_hdl);

				erase_free_handle();  // 删除已完成跟踪的线程的句柄
				printf("#frame: %d\n", g_cur_frame_idx);
			}
		}

		if( g_cur_frame_idx > 500 )
		{
			EnterCriticalSection(&cs);
			g_bworking = 0;
			LeaveCriticalSection(&cs);

			break;
		}
	}
	DWORD tcost = GetTickCount() - st;

	printf("tcost: %d\n", tcost);

	g_bworking = 0;   // 停止跟踪线程
	writer.release();

	EnterCriticalSection(&cs_hdl);

	for(int i = 0; i < g_thread_handle.size(); i++)
	{
		WaitForSingleObject( g_thread_handle[i], INFINITE);   //  等待跟踪线程退出
	}

	LeaveCriticalSection(&cs_hdl);
	DeleteCriticalSection(&cs);
	DeleteCriticalSection(&cs_hdl);

	int kkk = 0;

	return 0;
}

unsigned _stdcall track_single_obj(void *param)  // 跟踪单个目标
{
	int obj_id,frame_idx;
	rect pos, pos2;
	Mat det_frame = get_init_track_info(obj_id, pos, frame_idx); // 获取跟踪目标的初始信息（目标 id, 坐标位置， 帧的索引）

	Rect2d roi_rect,obj_rect, gobj_rect;// = g_boundingBox[obj_idx];
	Ptr<Tracker> tracker = Tracker::create( g_tracker_algorithm );

	gobj_rect.x = pos.xmin, gobj_rect.y = pos.ymin, 
	gobj_rect.width = pos.xmax - pos.xmin + 1, gobj_rect.height = pos.ymax - pos.ymin;

	Mat init_img = get_init_local_img(det_frame, roi_rect, gobj_rect); // 获取目标第一帧的局部图像，用于跟踪算法的初始化
	obj_rect = gobj_rect, obj_rect.x -= roi_rect.x, obj_rect.y -= roi_rect.y;
	tracker->init( init_img,  obj_rect);

	char vname[128];
	int delta = g_move_rate * g_maxsize;
	Mat frame, roi_img(2*delta+1, 2*delta+1, CV_8UC3);
	frame_idx++;
	for( ;; )
	{
		EnterCriticalSection(&cs);
		int btracking = g_bworking;
		LeaveCriticalSection(&cs);

		if( !btracking )
			break;

		if( obj_id == 7 )
			int kkkk = 0;

		int flag = get_next_frame(frame, frame_idx); // 获取下一帧图像
		if( flag )//frame_idx > pre_frame_idx /*&& btracking*/
		{
			get_next_local_pxls2(roi_img, frame, gobj_rect, roi_rect, g_maxsize); // 获取局部图像，用于跟踪
			int track_flag = tracker->update( roi_img, obj_rect );  // 跟踪目标在下一帧中的位置， 更新 obj_rect 的值

			if( !track_flag ) // 跟踪失败，准备退出线程
				break;

			if( obj_id == 7 )
				int jjjjjj = 0;

			gobj_rect.x = obj_rect.x + roi_rect.x, gobj_rect.y = obj_rect.y + roi_rect.y;
			gobj_rect.width = obj_rect.width, gobj_rect.height = obj_rect.height;

			pos.xmin = gobj_rect.x, pos.ymin = gobj_rect.y;
			pos.xmax = pos.xmin + gobj_rect.width - 1, pos.ymax = pos.ymin + obj_rect.height - 1;
			pos2 = pos, pos2.ymin += g_start_track_dist, pos2.ymax += g_start_track_dist;

			double rate = get_rect_overlap(&pos2, &g_track_roi);

			int i = 0, k = 0;
			if( /*rate > 0.0 &&*/ pos.ymax < g_track_roi.ymax )  // 是否在跟踪区域内
			{
				ObjectInfo tmp;
				tmp.obj_id = obj_id;
				tmp.pos = gobj_rect;

				EnterCriticalSection(&cs);

				for( i = 0; i < g_frame_buf.size(); i++)  // 将跟踪后的目标信息保存到缓冲区中
				{
					if( g_frame_buf[i].index != frame_idx )
						continue;

					if( !g_frame_buf[i].objs.size() )
					{
						g_frame_buf[i].objs.push_back(tmp);
					}
					else
					{
						for( k = 0; k < g_frame_buf[i].objs.size(); k++)
						{
							if( obj_id != g_frame_buf[i].objs[k].obj_id )
								continue;
							
							g_frame_buf[i].objs[k].pos = gobj_rect;
							break;
						}

						if( k == g_frame_buf[i].objs.size() )
							g_frame_buf[i].objs.push_back(tmp);
					}
				}

				LeaveCriticalSection(&cs);

				set_tracked_info(obj_id, gobj_rect, frame_idx);
			}

			if( pos.ymax > g_track_roi.ymax || pos.xmin <= g_track_roi.xmin || pos.xmax >= g_track_roi.xmax ) // 如果在跟踪区域外，则退出
				break;

			frame_idx++;
			//printf("#frame %d obj: %d tracked: %d\n", frame_idx, obj_idx, flag);
		}
	}

	EnterCriticalSection(&cs);

	for(int k = 0; k < g_obj_info.size(); k++)
	{
		if( g_obj_info[k].obj_id != obj_id )
			continue;

		g_obj_info.erase(g_obj_info.begin() + k); // 跟踪结束，删去该目标的信息
		break;
	}

	LeaveCriticalSection(&cs);

	return 1;
}

Mat get_init_track_info(int &obj_id, rect &pos, int &init_frame_idx)
{
	Mat frame;
	EnterCriticalSection(&cs);

	int det_idx;
	for(int i = 0; i < g_obj_info.size(); i++)
	{
		if( g_obj_info[i].flag == DETECTED )
		{
			g_obj_info[i].flag = TRACKING;
			obj_id = g_obj_info[i].obj_id;
			pos = g_obj_info[i].pos;
			det_idx = g_obj_info[i].det_idx;
			
			break;
		}
	}

	for(int i = 0; i < g_frame_buf.size(); i++)
	{
		if( g_frame_buf[i].index == det_idx )
			frame = g_frame_buf[i].frame;
	}

	LeaveCriticalSection(&cs);

	init_frame_idx = det_idx;
	return frame;
}

void set_tracked_info(int obj_id, Rect2d &pos, int tracked_idx)  // 更新
{
	EnterCriticalSection(&cs);

	for(int i = 0; i < g_obj_info.size(); i++)
	{
		if( g_obj_info[i].obj_id != obj_id )
			continue;

		g_obj_info[i].tracked_idx = tracked_idx; // 跟踪完第 tracked_idx 帧
		g_obj_info[i].pos.xmin = pos.x, g_obj_info[i].pos.ymin = pos.y;
		g_obj_info[i].pos.xmax = pos.x + pos.width - 1, g_obj_info[i].pos.ymax = pos.y + pos.height - 1;
	}

	LeaveCriticalSection(&cs);
}

int is_tracked()
{
	int flag = 0;

	EnterCriticalSection(&cs);

	if( g_obj_info.size() )
	{
		g_min_tracked_idx = g_obj_info[0].tracked_idx;
		for(int i = 1; i < g_obj_info.size(); i++)
		{
			if( g_min_tracked_idx > g_obj_info[i].tracked_idx )
				g_min_tracked_idx = g_obj_info[i].tracked_idx;
		}

		int ntracked = g_min_tracked_idx - g_frame_buf[0].index;
		if( ntracked >= g_ntracked || ( ntracked < g_ntracked && g_frame_buf.size() == g_frame_buf_size ) )// 判断缓冲区中的前 n 帧图像是否已经都跟踪完
		{
			flag = 1;
		}
	}

	if( g_frame_buf.size() < g_frame_buf_size )
		flag = 1;
	LeaveCriticalSection(&cs);

	return flag;
}

int get_min_tracked_idx()
{
	EnterCriticalSection(&cs);

	if( g_obj_info.size() )
	{
		g_min_tracked_idx = g_obj_info[0].tracked_idx;
		for(int i = 1; i < g_obj_info.size(); i++)  // 寻找已跟踪完的最小的帧索引号
		{
			if( g_min_tracked_idx > g_obj_info[i].tracked_idx )
				g_min_tracked_idx = g_obj_info[i].tracked_idx;
		}
	}
	else
		g_min_tracked_idx = g_cur_frame_idx;

	LeaveCriticalSection(&cs);

	return g_min_tracked_idx;
}

void set_min_tracked_idx(int index)
{
	EnterCriticalSection(&cs);
	if( index <= g_min_tracked_idx )
		g_min_tracked_idx = index;
	LeaveCriticalSection(&cs);
}

int get_next_frame(Mat &frame, int cur_idx)
{
	int i = 0, flag = 0;
	EnterCriticalSection(&cs);

	for(i = 0; i < g_frame_buf.size(); i++)
	{
		if( g_frame_buf[i].index == cur_idx )
		{
			//g_frame_buf[i].frame.copyTo(frame);
			frame = g_frame_buf[i].frame;
			flag = g_frame_buf[i].index;
			break;
		}
	}

	LeaveCriticalSection(&cs);

	return flag;
}

void get_next_local_pxls2(Mat& limg, Mat &src, Rect2d tar, Rect2d &adjust_rect, int maxsize)
{
	int delta = g_move_rate * g_maxsize;
	int center_x = tar.x + tar.width / 2;
	int center_y = tar.y + tar.height / 2;

	int l = center_x - delta, r = center_x + delta;
	int t = center_y - delta, b = center_y + delta;

	if( l < 0 )
	{
		r += abs(l);
		l = 0;
	}

	if( r >= src.cols )
	{
		l -= r - src.cols + 1;
		r = src.cols - 1;
	}

	if( t < 0 )
	{
		b += abs(t);
		t = 0;
	}

	if( b >= src.rows )
	{
		t -= b - src.rows + 1;
		b = src.rows - 1;
	}

	int c = src.channels();
	for(int i = t; i <= b; i++)
		for(int j = l; j <= r; j++)
		{
			for(int k = 0; k < c; k++)
				limg.at<uchar>((i-t), (j-l)*c+k) = src.at<uchar>(i,j*c+k);
		}

		adjust_rect.x = l, adjust_rect.y = t;
		adjust_rect.width = r - l + 1, adjust_rect.height = b - t + 1;
}

void copy_roi_pxls(Mat &dst, Mat &src, int top, int bottom, int left, int right)
{
	int i,j,k,c = src.channels();

	for(i = top; i <= bottom; i++)
		for(j = left; j <= right; j++)
		{
			for(k = 0; k < c; k++)
				dst.at<uchar>((i-top), (j-left)*c+k) = src.at<uchar>(i,j*c+k);
		}

}

Mat get_init_local_img(Mat src, Rect2d &adjust_rect, Rect2d pos)
{
	Mat limg;

	Rect2d tar = pos;//g_boundingBox[obj_idx];

	int delta = g_move_rate * g_maxsize;
	int center_x = tar.x + tar.width / 2;
	int center_y = tar.y + tar.height / 2;

	int l = center_x - delta, r = center_x + delta;
	int t = center_y - delta, b = center_y + delta;

	limg.create(2*delta+1, 2*delta+1, CV_8UC3);
	if( l < 0 )
	{
		r += abs(l);
		l = 0;
	}

	if( r >= src.cols )
	{
		l -= r - src.cols + 1;
		r = src.cols - 1;
	}

	if( t < 0 )
	{
		b += abs(t);
		t = 0;
	}

	if( b >= src.rows )
	{
		t -= b - src.rows + 1;
		b = src.rows - 1;
	}

	int c = src.channels();
	for(int i = t; i <= b; i++)
		for(int j = l; j <= r; j++)
		{
			for(int k = 0; k < c; k++)
				limg.at<uchar>((i-t), (j-l)*c+k) = src.at<uchar>(i,j*c+k);
		}

		adjust_rect.x = l, adjust_rect.y = t;
		adjust_rect.width = r - l + 1, adjust_rect.height = b - t + 1;

		return limg;
}

double get_rect_overlap(rect* R1,rect* R2)
{
	int cxR1,cxR2, cyR1,cyR2,wR1,wR2,hR1,hR2;
	cxR1=max(R1->xmax,R2->xmax);
	cxR2=min(R1->xmin,R2->xmin);
	wR1=R1->xmax-R1->xmin;
	wR2=R2->xmax-R2->xmin;    
	cyR1=max(R1->ymax,R2->ymax);
	cyR2=min(R1->ymin,R2->ymin);
	hR1=R1->ymax-R1->ymin;
	hR2=R2->ymax-R2->ymin;
	int w=wR1+wR2-(cxR1-cxR2);
	int h=hR1+hR2-(cyR1-cyR2);
	if(w<0) w=0;if(h<0) h=0;

	double mina = wR1*hR1 < wR2*hR2 ? wR1*hR1 : wR2*hR2;
	double rate = w*h / mina;
	return rate;
}

void detect_face( Mat& img, CascadeClassifier& cascade, vector<rect> &faces )
{
	int i = 0;
	double t = 0;
	vector<Rect> tmp;

	Mat gray, smallImg( cvRound (img.rows), cvRound(img.cols), CV_8UC1 );//将图片缩小，加快检测速度

	cvtColor( img, gray, CV_BGR2GRAY );//因为用的是类haar特征，所以都是基于灰度图像的，这里要转换成灰度图像
//	resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );//将尺寸缩小到1/scale,用线性插值
	equalizeHist( gray, gray );//直方图均衡

//	t = (double)cvGetTickCount();//用来计算算法执行时间

	cascade.detectMultiScale( gray, tmp,
		1.1, 5, 0
		//|CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		//|CV_HAAR_SCALE_IMAGE
		,
		Size(18, 18), Size(100,100) );

	//erase_fn_using_histgram(g_background, img, tmp);

// 	t = (double)cvGetTickCount() - t;//相减为算法执行的时间
// 	printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
// 	for( vector<Rect>::const_iterator r = tmp.begin(); r != tmp.end(); r++, i++ )
// 	{
// 		Scalar color = CV_RGB(255,255,0);//colors[i%8];
// 		Point pt1,pt2;
// 		pt1.x = tmp[i].x, pt1.y = tmp[i].y;
// 		pt2.x = tmp[i].x + tmp[i].width, pt2.y = tmp[i].y + tmp[i].height;
// 		//rectangle(img,pt1, pt2, color, 2);
// 	}

	for(i = 0; i < tmp.size(); i++)
	{
		rect face;
		face.xmin = tmp[i].x, face.xmax = face.xmin + tmp[i].width - 1;
		face.ymin = tmp[i].y, face.ymax = face.ymin + tmp[i].height - 1;
		faces.push_back(face);
	}

	//imwrite("result.bmp", img);
}

void smooth_detected( vector<rect> &objs, int width, int height, Obj_vec_que &queue, Loc_Obj_map &trace )
{
	vector<Object> detected;
	for(int i = 0; i < objs.size(); i++)
	{
		Object new_obj;
		new_obj.frame_index = 1;//idx;
		int w = /*faces[i].xmax - faces[i].xmin*/width, h = height;/*faces[i].ymax - faces[i].ymin*/;
		int cx = (objs[i].xmin + objs[i].xmax)/2, cy = (objs[i].ymin + objs[i].ymax)/2;
		Loc new_loc(cx, cy, w, h);
		new_obj.loc = new_loc;
		new_obj.scale = objs[i].xmax - objs[i].xmin + 1;//20;
		detected.push_back(new_obj);
	}

	vector<Object> confirmed;

	produce_confirmed_object(detected, confirmed, queue, trace);

	vector<rect> tmp;
	for(int i = 0; i < confirmed.size(); i++)
	{
		Object new_obj = confirmed[i];
		rect face;
		face.xmin = new_obj.loc.x - 0.5*new_obj.scale;
		face.ymin = new_obj.loc.y - 0.5*new_obj.scale;
		face.xmax = face.xmin + 1*new_obj.scale - 1;
		face.ymax = face.ymin + 1*new_obj.scale - 1;

		tmp.push_back(face);
	}

	objs = tmp;
}

int push_new_detected( vector<rect> &objs, int frame_idx, int &obj_cnt )
{
	int i = 0,j = 0,num = 0;
	EnterCriticalSection(&cs);

	for(i = 0; i < objs.size(); i++)
	{
		if( g_obj_info.size() )
		{
			for(j = 0; j < g_obj_info.size(); j++)
			{
				double rate = get_rect_overlap(&objs[i], &g_obj_info[j].pos);

				if( rate < 0.000000001 )
					continue;
				else
					break;
			}

			if( j == g_obj_info.size() && objs[i].ymax < g_track_roi.ymin &&
				objs[i].ymax + g_start_track_dist > g_track_roi.ymin )
			{
				ObjectTrack obj;
				obj.flag = DETECTED;
				obj.obj_id = ++obj_cnt;
				obj.det_idx = frame_idx;
				obj.pos = objs[i];
				obj.tracked_idx = g_cur_frame_idx - 1;
				g_obj_info.push_back(obj);

				num++;
				break;
			}
		}
		else
		{
			if( objs[i].ymax < g_track_roi.ymin &&
				objs[i].ymax + g_start_track_dist > g_track_roi.ymin )
			{
				ObjectTrack obj;
				obj.flag = DETECTED;
				obj.obj_id = ++obj_cnt;
				obj.det_idx = frame_idx;
				obj.pos = objs[i];
				obj.tracked_idx = g_cur_frame_idx - 1;
				g_obj_info.push_back(obj);

				num++;
				break;
			}
		}

		if( !g_obj_info.size() )
		{
			for(int k = 0; k < g_frame_buf.size(); k++)
			{
				if( frame_idx == g_frame_buf[k].index )
				{
					ObjectInfo tmp;
					tmp.obj_id = obj_cnt;
					tmp.pos.x = objs[i].xmin, tmp.pos.y = objs[i].ymin;
					tmp.pos.width = objs[i].xmax - objs[i].xmin + 1;
					tmp.pos.height = objs[i].xmax - objs[i].xmin + 1;

					g_frame_buf[k].objs.push_back(tmp);
					break;
				}
			}
		}
	}

	LeaveCriticalSection(&cs);

	return num;
}

void erase_free_handle()
{
	int i = 0;
	vector<HANDLE> tmp;
	EnterCriticalSection(&cs_hdl);

	for(i = 0; i < g_thread_handle.size(); i++)
	{
		DWORD exitcode;
		GetExitCodeThread(g_thread_handle[i], &exitcode);

		if( exitcode == STILL_ACTIVE )
			tmp.push_back(g_thread_handle[i]);
	}

	g_thread_handle = tmp;
	LeaveCriticalSection(&cs_hdl);
}